local gears = require("gears")
local awful = require("awful")
local xresources = require("beautiful").xresources
local dpi = xresources.apply_dpi

local theme
pcall(function() theme = dofile("/usr/share/awesome/themes/xresources/theme.lua") end)


local theme_dir = gears.filesystem.get_dir("config") .. "/theme.lua"

theme.border_width = dpi(4)
theme.useless_gap = dpi(1000000)


return theme
