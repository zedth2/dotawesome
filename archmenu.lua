 local menue0e4fc6213e8b3593495a7260c3a4c2e = {
     {"KMag", "kmag", "/usr/share/icons/hicolor/16x16/apps/kmag.png" },
     {"KMouseTool", "kmousetool -qwindowtitle KMouseTool", "/usr/share/icons/hicolor/16x16/apps/kmousetool.png" },
     {"KMouth", "kmouth", "/usr/share/icons/hicolor/16x16/apps/kmouth.png" },
 }

 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"Archive Manager", "file-roller "},
     {"Ark", "ark ", "/usr/share/icons/hicolor/48x48/apps/ark.png" },
     {"Character Map", "gucharmap", "/usr/share/icons/gnome/16x16/apps/accessories-character-map.png" },
     {"ClamTk", "clamtk ", "/usr/share/pixmaps/clamtk.png" },
     {"Code - OSS", "/usr/bin/code-oss --no-sandbox --unity-launch ", "/usr/share/pixmaps/com.visualstudio.code.oss.png" },
     {"DB Browser for SQLite", "sqlitebrowser ", "/usr/share/icons/hicolor/256x256/apps/sqlitebrowser.png" },
     {"Docky", "docky", "/usr/share/icons/hicolor/16x16/apps/docky.svg" },
     {"Double Commander", "doublecmd ", "/usr/share/pixmaps/doublecmd.png" },
     {"Elementary Perf", "elementary_perf", "/usr/share/icons/hicolor/128x128/apps/elementary.png" },
     {"Elementary Test", "elementary_test", "/usr/share/icons/hicolor/128x128/apps/elementary.png" },
     {"Engrampa Archive Manager", "engrampa ", "/usr/share/icons/hicolor/16x16/apps/engrampa.png" },
     {"Filelight", "filelight ", "/usr/share/icons/hicolor/16x16/apps/filelight.png" },
     {"KAlarm", "kalarm", "/usr/share/icons/hicolor/16x16/apps/kalarm.png" },
     {"KBackup", "kbackup ", "/usr/share/icons/hicolor/16x16/apps/kbackup.png" },
     {"KCalc", "kcalc", "/usr/share/icons/gnome/16x16/apps/accessories-calculator.png" },
     {"KCharSelect", "kcharselect --qwindowtitle KCharSelect", "/usr/share/icons/gnome/16x16/apps/accessories-character-map.png" },
     {"KDE Itinerary", "itinerary ", "/usr/share/icons/hicolor/48x48/apps/itinerary.svg" },
     {"KFloppy", "kfloppy -qwindowtitle KFloppy", "/usr/share/icons/hicolor/16x16/apps/kfloppy.png" },
     {"KGpg", "kgpg ", "/usr/share/icons/hicolor/16x16/apps/kgpg.png" },
     {"KMag", "kmag", "/usr/share/icons/hicolor/16x16/apps/kmag.png" },
     {"KMail Import Wizard", "akonadiimportwizard", "/usr/share/icons/hicolor/64x64/apps/kontact-import-wizard.png" },
     {"KMouseTool", "kmousetool -qwindowtitle KMouseTool", "/usr/share/icons/hicolor/16x16/apps/kmousetool.png" },
     {"KMouth", "kmouth", "/usr/share/icons/hicolor/16x16/apps/kmouth.png" },
     {"KNotes", "knotes", "/usr/share/icons/hicolor/16x16/apps/knotes.png" },
     {"KTeaTime", "kteatime", "/usr/share/icons/hicolor/16x16/apps/kteatime.png" },
     {"KTimer", "ktimer", "/usr/share/icons/hicolor/16x16/apps/ktimer.png" },
     {"KWrite", "kwrite ", "/usr/share/icons/hicolor/16x16/apps/kwrite.png" },
     {"Kate", "kate -b ", "/usr/share/icons/hicolor/16x16/apps/kate.png" },
     {"KeePassXC", "keepassxc ", "/usr/share/icons/hicolor/256x256/apps/keepassxc.png" },
     {"Kleopatra", "kleopatra", "/usr/share/icons/hicolor/16x16/apps/kleopatra.png" },
     {"Klipper", "klipper"},
     {"Leafpad", "leafpad ", "/usr/share/icons/hicolor/16x16/apps/leafpad.png" },
     {"Lessmsi", "lessmsi-gui"},
     {"PCManFM-Qt File Manager", "pcmanfm-qt ", "/usr/share/icons/gnome/16x16/apps/system-file-manager.png" },
     {"Screenshot", "gnome-screenshot --interactive"},
     {"Shutter", "shutter ", "/usr/share/icons/hicolor/16x16/apps/shutter.png" },
     {"SpaceFM File Search", "spacefm --find-files ", "/usr/share/icons/hicolor/48x48/apps/spacefm-find.png" },
     {"Spectacle", "/usr/bin/spectacle", "/usr/share/icons/hicolor/16x16/apps/spectacle.png" },
     {"Sweeper", "sweeper"},
     {"To Do", "gnome-todo", "/usr/share/icons/hicolor/16x16/apps/org.gnome.Todo.png" },
     {"Vim", "xterm -e vim ", "/usr/share/icons/hicolor/48x48/apps/gvim.png" },
     {"Xarchiver", "xarchiver ", "/usr/share/icons/hicolor/16x16/apps/xarchiver.png" },
     {"compton", "compton", "/usr/share/icons/hicolor/48x48/apps/compton.png" },
     {"nitrogen", "nitrogen", "/usr/share/icons/hicolor/16x16/apps/nitrogen.png" },
     {"picom", "picom"},
 }

 local menude7a22a0c94aa64ba2449e520aa20c99 = {
     {"Artikulate", "artikulate -qwindowtitle \"Artikulate\" ", "/usr/share/icons/hicolor/16x16/apps/artikulate.png" },
     {"Blinken", "blinken", "/usr/share/icons/hicolor/16x16/apps/blinken.png" },
     {"Cantor", "cantor -qwindowicon cantor -qwindowtitle Cantor ", "/usr/share/icons/hicolor/16x16/apps/cantor.png" },
     {"KAlgebra", "kalgebra ", "/usr/share/icons/hicolor/64x64/apps/kalgebra.png" },
     {"KAlgebra Mobile", "kalgebramobile ", "/usr/share/icons/hicolor/64x64/apps/kalgebra.png" },
     {"KBruch", "kbruch -qwindowtitle KBruch -qwindowicon kbruch", "/usr/share/icons/hicolor/16x16/apps/kbruch.png" },
     {"KDE Marble", "marble ", "/usr/share/icons/hicolor/16x16/apps/marble.png" },
     {"KGeography", "kgeography", "/usr/share/icons/hicolor/16x16/apps/kgeography.png" },
     {"KHangMan", "khangman -qwindowtitle \"KHangMan\" ", "/usr/share/icons/hicolor/16x16/apps/khangman.png" },
     {"KLettres", "klettres -qwindowtitle KLettres -qwindowicon klettres", "/usr/share/icons/hicolor/16x16/apps/klettres.png" },
     {"KTouch", "ktouch", "/usr/share/icons/hicolor/16x16/apps/ktouch.png" },
     {"KTurtle", "kturtle -qwindowtitle KTurtle", "/usr/share/icons/hicolor/16x16/apps/kturtle.png" },
     {"KWordQuiz", "kwordquiz  -qwindowtitle KWordQuiz -qwindowicon kwordquiz", "/usr/share/icons/hicolor/16x16/apps/kwordquiz.png" },
     {"Kalzium", "kalzium -qwindowtitle Kalzium", "/usr/share/icons/hicolor/16x16/apps/kalzium.png" },
     {"Kanagram", "kanagram", "/usr/share/icons/hicolor/16x16/apps/kanagram.png" },
     {"Kig", "kig  --qwindowtitle Kig", "/usr/share/icons/hicolor/16x16/apps/kig.png" },
     {"Kiten", "kiten -qwindowtitle Kiten -qwindowicon kiten", "/usr/share/icons/hicolor/16x16/apps/kiten.png" },
     {"KmPlot", "kmplot ", "/usr/share/icons/hicolor/16x16/apps/kmplot.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"Minuet", "minuet", "/usr/share/icons/hicolor/16x16/apps/minuet.png" },
     {"Parley", "parley ", "/usr/share/icons/hicolor/16x16/apps/parley.png" },
     {"Rocs", "rocs  -qwindowtitle Rocs", "/usr/share/icons/hicolor/16x16/apps/rocs.png" },
     {"Step", "step -qwindowtitle Step -qwindowicon step", "/usr/share/icons/hicolor/16x16/apps/step.png" },
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"Blinken", "blinken", "/usr/share/icons/hicolor/16x16/apps/blinken.png" },
     {"Bomber", "bomber -qwindowtitle Bomber", "/usr/share/icons/hicolor/32x32/apps/bomber.png" },
     {"Bovo", "bovo -qwindowtitle Bovo", "/usr/share/icons/hicolor/16x16/apps/bovo.png" },
     {"Dosbox", "dosbox", "/usr/share/pixmaps/dosbox.png" },
     {"GemRB", "xterm -e gemrb", "/usr/share/pixmaps/gemrb.png" },
     {"Granatier", "granatier", "/usr/share/icons/hicolor/16x16/apps/granatier.png" },
     {"KAtomic", "katomic -qwindowtitle KAtomic", "/usr/share/icons/hicolor/16x16/apps/katomic.png" },
     {"KBlackBox", "kblackbox -qwindowtitle KBlackBox", "/usr/share/icons/hicolor/16x16/apps/kblackbox.png" },
     {"KBlocks", "kblocks -qwindowtitle KBlocks", "/usr/share/icons/hicolor/16x16/apps/kblocks.png" },
     {"KBounce", "kbounce -qwindowtitle KBounce", "/usr/share/icons/hicolor/16x16/apps/kbounce.png" },
     {"KBreakOut", "kbreakout -qwindowtitle KBreakOut", "/usr/share/icons/hicolor/16x16/apps/kbreakout.png" },
     {"KDiamond", "kdiamond -qwindowtitle KDiamond", "/usr/share/icons/hicolor/16x16/apps/kdiamond.png" },
     {"KFourInLine", "kfourinline", "/usr/share/icons/hicolor/16x16/apps/kfourinline.png" },
     {"KGoldrunner", "kgoldrunner", "/usr/share/icons/hicolor/16x16/apps/kgoldrunner.png" },
     {"KHangMan", "khangman -qwindowtitle \"KHangMan\" ", "/usr/share/icons/hicolor/16x16/apps/khangman.png" },
     {"KJumpingCube", "kjumpingcube -qwindowtitle KJumpingCube", "/usr/share/icons/hicolor/16x16/apps/kjumpingcube.png" },
     {"KMahjongg", "kmahjongg -qwindowtitle KMahjongg", "/usr/share/icons/hicolor/16x16/apps/kmahjongg.png" },
     {"KMines", "kmines -qwindowtitle KMines", "/usr/share/icons/hicolor/16x16/apps/kmines.png" },
     {"KNetWalk", "knetwalk -qwindowtitle KNetWalk", "/usr/share/icons/hicolor/16x16/apps/knetwalk.png" },
     {"KPatience", "kpat -qwindowtitle KPatience ", "/usr/share/icons/hicolor/16x16/apps/kpat.png" },
     {"KReversi", "kreversi -qwindowtitle KReversi", "/usr/share/icons/hicolor/16x16/apps/kreversi.png" },
     {"KSnakeDuel", "ksnakeduel", "/usr/share/icons/hicolor/16x16/apps/ksnakeduel.png" },
     {"KSpaceDuel", "kspaceduel -qwindowtitle KSpaceDuel", "/usr/share/icons/hicolor/16x16/apps/kspaceduel.png" },
     {"KSquares", "ksquares -qwindowicon ksquares -qwindowtitle KSquares", "/usr/share/icons/hicolor/16x16/apps/ksquares.png" },
     {"KSudoku", "ksudoku -qwindowicon ksudoku -qwindowtitle KSudoku", "/usr/share/icons/hicolor/16x16/apps/ksudoku.png" },
     {"Kajongg", "kajongg", "/usr/share/icons/hicolor/16x16/apps/kajongg.png" },
     {"Kanagram", "kanagram", "/usr/share/icons/hicolor/16x16/apps/kanagram.png" },
     {"Kapman", "kapman", "/usr/share/icons/hicolor/16x16/apps/kapman.png" },
     {"Kigo", "kigo  -qwindowicon kigo -qwindowtitle Kigo", "/usr/share/icons/hicolor/16x16/apps/kigo.png" },
     {"Killbots", "killbots -qwindowtitle Killbots", "/usr/share/icons/hicolor/16x16/apps/killbots.png" },
     {"Kiriki", "kiriki -qwindowtitle Kiriki", "/usr/share/icons/hicolor/16x16/apps/kiriki.png" },
     {"Klickety", "klickety -qwindowtitle Klickety", "/usr/share/icons/hicolor/16x16/apps/klickety.png" },
     {"Knights", "knights  -qwindowtitle Knights", "/usr/share/icons/hicolor/16x16/apps/knights.png" },
     {"Kolf", "kolf ", "/usr/share/icons/hicolor/16x16/apps/kolf.png" },
     {"Kollision", "kollision", "/usr/share/icons/hicolor/16x16/apps/kollision.png" },
     {"Kolor Lines", "klines -qwindowtitle Kolor Lines", "/usr/share/icons/hicolor/16x16/apps/klines.png" },
     {"Konquest", "konquest -qwindowtitle Konquest", "/usr/share/icons/hicolor/16x16/apps/konquest.png" },
     {"KsirK", "ksirk -qwindowtitle KsirK -qwindowicon ksirk", "/usr/share/icons/hicolor/16x16/apps/ksirk.png" },
     {"KsirK Skin Editor", "ksirkskineditor -qwindowtitle KsirK Skin Editor -qwindowicon preferences-desktop-locale", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-locale.png" },
     {"Kubrick", "kubrick", "/usr/share/icons/hicolor/16x16/apps/kubrick.png" },
     {"LSkat", "lskat", "/usr/share/icons/hicolor/16x16/apps/lskat.png" },
     {"Lutris", "lutris ", "/usr/share/icons/hicolor/16x16/apps/lutris.png" },
     {"Naval Battle", "knavalbattle -qwindowtitle Naval Battle ", "/usr/share/icons/hicolor/16x16/apps/knavalbattle.png" },
     {"OpenMW Content Editor", "openmw-cs", "/usr/share/pixmaps/openmw-cs.png" },
     {"OpenMW Launcher", "openmw-launcher", "/usr/share/pixmaps/openmw.png" },
     {"OpenRCT2", "openrct2 ", "/usr/share/icons/hicolor/16x16/apps/openrct2.png" },
     {"OpenTTD", "openttd", "/usr/share/icons/hicolor/16x16/apps/openttd.png" },
     {"PCSXR", "pcsxr", "/usr/share/pixmaps/pcsxr-icon.png" },
     {"PPSSPP (Qt)", "PPSSPPQt ", "/usr/share/icons/hicolor/16x16/apps/ppsspp.png" },
     {"PPSSPP (SDL)", "PPSSPPSDL ", "/usr/share/icons/hicolor/16x16/apps/ppsspp.png" },
     {"Palapeli", "palapeli ", "/usr/share/icons/hicolor/16x16/apps/palapeli.png" },
     {"Potato Guy", "ktuberling -qwindowtitle Potato Guy ", "/usr/share/icons/hicolor/16x16/apps/ktuberling.png" },
     {"RetroArch", "retroarch", "/usr/share/pixmaps/retroarch.svg" },
     {"RuneLite", "runelite", "/usr/share/pixmaps/runelite.png" },
     {"SameGame", "klickety --KSameMode -qwindowtitle SameGame", "/usr/share/icons/hicolor/16x16/apps/ksame.png" },
     {"Shisen-Sho", "kshisen -qwindowtitle Shisen-Sho", "/usr/share/icons/hicolor/16x16/apps/kshisen.png" },
     {"Steam (Runtime)", "/usr/bin/steam-runtime ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Tux Paint", "tuxpaint", "/usr/share/pixmaps/tuxpaint.png" },
     {"VBA-M", "visualboyadvance-m ", "/usr/share/icons/hicolor/16x16/apps/vbam.png" },
     {"Zsnes", "zsnes", "/usr/share/pixmaps/zsnes.png" },
     {"bsnes", "bsnes", "/usr/share/icons/bsnes.png" },
     {"ePSXe", "/usr/bin/epsxe", "///usr/share/pixmaps/epsxe.png" },
     {"picmi", "picmi", "/usr/share/icons/hicolor/16x16/apps/picmi.png" },
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"Document Viewer", "evince "},
     {"E-book viewer", "ebook-viewer --detach ", "/usr/share/icons/hicolor/16x16/apps/calibre-viewer.png" },
     {"FontForge", "fontforge ", "/usr/share/icons/hicolor/16x16/apps/org.fontforge.FontForge.png" },
     {"GNU Image Manipulation Program", "gimp-2.10 ", "/usr/share/icons/hicolor/16x16/apps/gimp.png" },
     {"GNU Paint", "gpaint"},
     {"Gpick", "gpick", "/usr/share/icons/hicolor/48x48/apps/gpick.png" },
     {"Gwenview", "gwenview ", "/usr/share/icons/hicolor/16x16/apps/gwenview.png" },
     {"KColorChooser", "kcolorchooser", "/usr/share/icons/hicolor/16x16/apps/kcolorchooser.png" },
     {"KOReader", "/opt/appimages/koreader.AppImage ", "/usr/share/pixmaps/koreader.png" },
     {"KRuler", "kruler", "/usr/share/icons/hicolor/16x16/apps/kruler.png" },
     {"KolourPaint", "kolourpaint ", "/usr/share/icons/hicolor/16x16/apps/kolourpaint.png" },
     {"Kontrast", "kontrast"},
     {"Krita", "krita ", "/usr/share/icons/hicolor/16x16/apps/krita.png" },
     {"LRF viewer", "lrfviewer ", "/usr/share/icons/hicolor/16x16/apps/calibre-viewer.png" },
     {"LXImage", "lximage-qt ", "/usr/share/icons/hicolor/48x48/apps/lximage-qt.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"Okular", "okular ", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
     {"Peek", "peek"},
     {"Pinta", "pinta ", "/usr/share/icons/hicolor/16x16/apps/pinta.png" },
     {"Screenshot", "lximage-qt --screenshot", "/usr/share/icons/gnome/16x16/devices/camera-photo.png" },
     {"Shotwell", "shotwell ", "/usr/share/icons/hicolor/16x16/apps/shotwell.png" },
     {"Tux Paint", "tuxpaint", "/usr/share/pixmaps/tuxpaint.png" },
     {"Viewnior", "viewnior ", "/usr/share/icons/hicolor/16x16/apps/viewnior.png" },
     {"XDvi", "xdvi "},
     {"XPaint", "xpaint", "///usr/share/pixmaps/XPaintIcon.xpm" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Akregator", "akregator ", "/usr/share/icons/hicolor/16x16/apps/akregator.png" },
     {"Avahi SSH Server Browser", "/usr/bin/bssh", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Avahi VNC Server Browser", "/usr/bin/bvnc", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Chromium", "/usr/bin/chromium ", "/usr/share/icons/hicolor/16x16/apps/chromium.png" },
     {"Contact Print Theme Editor", "contactprintthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Contact Theme Editor", "contactthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Deluge", "deluge-gtk ", "/usr/share/icons/hicolor/16x16/apps/deluge.png" },
     {"Dropbox", "dropbox", "/usr/share/pixmaps/dropbox.svg" },
     {"Firefox", "/usr/lib/firefox/firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"Fondo", "com.github.calo001.fondo", "/usr/share/icons/hicolor/16x16/apps/com.github.calo001.fondo.svg" },
     {"IM Contacts", "ktp-contactlist ", "/usr/share/icons/hicolor/16x16/apps/telepathy-kde.png" },
     {"IceCat", "/usr/bin/icecat ", "/usr/share/icons/hicolor/16x16/apps/icecat.png" },
     {"IceCat - Safe mode", "/usr/bin/icecat -safe-mode ", "/usr/share/icons/hicolor/16x16/apps/icecat.png" },
     {"KDE Connect", "kdeconnect-app"},
     {"KDE Connect Indicator", "kdeconnect-indicator"},
     {"KDE Connect SMS", "kdeconnect-sms"},
     {"KDE IM Log Viewer", "ktp-log-viewer "},
     {"KGet", "kget -qwindowtitle KGet ", "/usr/share/icons/hicolor/16x16/apps/kget.png" },
     {"KMail", "kmail -qwindowtitle KMail ", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KMail Header Theme Editor", "headerthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KRDC", "krdc -qwindowtitle KRDC "},
     {"KTnef", "ktnef ", "/usr/share/icons/hicolor/48x48/apps/ktnef.png" },
     {"KTorrent", "ktorrent ", "/usr/share/icons/hicolor/16x16/apps/ktorrent.png" },
     {"Konqueror", "konqueror", "/usr/share/icons/hicolor/16x16/apps/konqueror.png" },
     {"Konversation", "konversation -qwindowtitle Konversation ", "/usr/share/icons/hicolor/16x16/apps/konversation.png" },
     {"Kopete", "kopete -qwindowtitle Kopete ", "/usr/share/icons/hicolor/16x16/apps/kopete.png" },
     {"Krfb", "krfb -qwindowtitle Krfb ", "/usr/share/icons/hicolor/48x48/apps/krfb.png" },
     {"Liferea", "liferea ", "/usr/share/icons/hicolor/16x16/apps/net.sourceforge.liferea.png" },
     {"MEGAsync", "megasync", "/usr/share/icons/hicolor/16x16/apps/mega.png" },
     {"Open on connected device via KDE Connect", "kdeconnect-handler --open "},
     {"PIM Data Exporter", "pimdataexporter", "/usr/share/icons/hicolor/16x16/apps/kontact.png" },
     {"QuiteRSS", "quiterss", "/usr/share/icons/hicolor/16x16/apps/quiterss.png" },
     {"RSS Guard", "rssguard"},
     {"Sieve Editor", "sieveeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"Signal", "signal-desktop", "/usr/share/icons/hicolor/16x16/apps/signal-desktop.png" },
     {"Steam (Runtime)", "/usr/bin/steam-runtime ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Tor Browser (en-US)", "/usr/bin/tor-browser ", "/usr/share/pixmaps/tor-browser.png" },
     {"Vivaldi", "/usr/bin/vivaldi-stable ", "/usr/share/icons/hicolor/16x16/apps/vivaldi.png" },
     {"qutebrowser", "qutebrowser ", "/usr/share/icons/hicolor/16x16/apps/qutebrowser.png" },
     {"unison", "unison-gtk2", "/usr/share/icons/hicolor/16x16/apps/unison.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"Bookworm", "com.github.babluboy.bookworm ", "/usr/share/icons/hicolor/16x16/apps/com.github.babluboy.bookworm.svg" },
     {"Calibre", "calibre --detach ", "/usr/share/icons/hicolor/16x16/apps/calibre-gui.png" },
     {"Contact Print Theme Editor", "contactprintthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Contact Theme Editor", "contactthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"Document Viewer", "evince "},
     {"E-book editor", "ebook-edit --detach ", "/usr/share/icons/hicolor/16x16/apps/calibre-ebook-edit.png" },
     {"GnuCash", "gnucash ", "/usr/share/icons/hicolor/16x16/apps/gnucash-icon.png" },
     {"KAddressBook", "kaddressbook ", "/usr/share/icons/hicolor/16x16/apps/kaddressbook.png" },
     {"KMail", "kmail -qwindowtitle KMail ", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KMail Header Theme Editor", "headerthemeeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"KOrganizer", "korganizer ", "/usr/share/icons/hicolor/16x16/apps/korganizer.png" },
     {"KTnef", "ktnef ", "/usr/share/icons/hicolor/48x48/apps/ktnef.png" },
     {"Kontact", "kontact", "/usr/share/icons/hicolor/16x16/apps/kontact.png" },
     {"LibreOffice", "libreoffice ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-startcenter.png" },
     {"LibreOffice Base", "libreoffice --base ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-base.png" },
     {"LibreOffice Calc", "libreoffice --calc ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-calc.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LibreOffice Impress", "libreoffice --impress ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-impress.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"LibreOffice Writer", "libreoffice --writer ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png" },
     {"Lokalize", "lokalize ", "/usr/share/icons/hicolor/32x32/apps/lokalize.png" },
     {"Okular", "okular ", "/usr/share/icons/hicolor/16x16/apps/okular.png" },
     {"Sieve Editor", "sieveeditor", "/usr/share/icons/hicolor/16x16/apps/kmail.png" },
     {"Xpdf", "xpdf ", "/usr/share/pixmaps/xpdf.svg" },
     {"ePDFViewer", "epdfview ", "/usr/share/icons/hicolor/24x24/apps/epdfview.png" },
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"Akonadi Console", "akonadiconsole", "/usr/share/icons/hicolor/16x16/apps/akonadiconsole.png" },
     {"Android Studio", "android-studio ", "/usr/share/pixmaps/android-studio.png" },
     {"Builder", "gnome-builder "},
     {"CMake", "cmake-gui ", "/usr/share/icons/hicolor/32x32/apps/CMakeSetup.png" },
     {"Cervisia", "cervisia -qwindowtitle Cervisia ", "/usr/share/icons/hicolor/16x16/apps/cervisia.png" },
     {"Code - OSS", "/usr/bin/code-oss --no-sandbox --unity-launch ", "/usr/share/pixmaps/com.visualstudio.code.oss.png" },
     {"Cuttlefish", "cuttlefish"},
     {"DB Browser for SQLite", "sqlitebrowser ", "/usr/share/icons/hicolor/256x256/apps/sqlitebrowser.png" },
     {"Devhelp", "devhelp"},
     {"DlangIDE", "dlangide", "/usr/share/pixmaps/dlangui-logo1.png" },
     {"Electron", "electron ", "/usr/share/pixmaps/electron.png" },
     {"Electron 9", "electron9 ", "/usr/share/pixmaps/electron9.png" },
     {"Elementary Perf", "elementary_perf", "/usr/share/icons/hicolor/128x128/apps/elementary.png" },
     {"Elementary Test", "elementary_test", "/usr/share/icons/hicolor/128x128/apps/elementary.png" },
     {"Emacs", "emacs ", "/usr/share/icons/hicolor/16x16/apps/emacs.png" },
     {"GdaBrowser", "gda-browser-5.0", "/usr/share/pixmaps/gda-browser-5.0.png" },
     {"Geany", "geany ", "/usr/share/icons/hicolor/16x16/apps/geany.png" },
     {"Glade", "glade "},
     {"IntelliJ IDEA Community Edition", "/usr/bin/idea ", "/usr/share/pixmaps/idea.png" },
     {"KAppTemplate", "kapptemplate", "/usr/share/icons/hicolor/16x16/apps/kapptemplate.png" },
     {"KCachegrind", "kcachegrind -qwindowtitle KCachegrind ", "/usr/share/icons/hicolor/32x32/apps/kcachegrind.png" },
     {"KDevelop", "kdevelop", "/usr/share/icons/hicolor/16x16/apps/kdevelop.png" },
     {"KDevelop (Pick Session)", "kdevelop --ps", "/usr/share/icons/hicolor/16x16/apps/kdevelop.png" },
     {"KImageMapEditor", "kimagemapeditor -qwindowtitle KImageMapEditor  ", "/usr/share/icons/hicolor/16x16/apps/kimagemapeditor.png" },
     {"KUIViewer", "kuiviewer -qwindowtitle KUIViewer ", "/usr/share/icons/hicolor/16x16/apps/kuiviewer.png" },
     {"Kirigami Gallery", "kirigami2gallery", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-theme.png" },
     {"Kompare", "kompare -o ", "/usr/share/icons/hicolor/16x16/apps/kompare.png" },
     {"Lokalize", "lokalize ", "/usr/share/icons/hicolor/32x32/apps/lokalize.png" },
     {"Meld", "meld ", "/usr/share/icons/hicolor/16x16/apps/org.gnome.meld.png" },
     {"Plasma Engine Explorer", "plasmaengineexplorer"},
     {"Plasma Global Theme Explorer", "lookandfeelexplorer", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-theme.png" },
     {"Plasma Theme Explorer", "plasmathemeexplorer", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-theme.png" },
     {"Qt Assistant", "assistant", "/usr/share/icons/hicolor/32x32/apps/assistant.png" },
     {"Qt Creator", "qtcreator ", "/usr/share/icons/hicolor/16x16/apps/QtProject-qtcreator.png" },
     {"Qt Designer", "designer ", "/usr/share/icons/hicolor/128x128/apps/QtProject-designer.png" },
     {"Qt Linguist", "linguist ", "/usr/share/icons/hicolor/16x16/apps/linguist.png" },
     {"Qt QDbusViewer ", "qdbusviewer", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer.png" },
     {"Robo 3T", "robo3t", "/usr/share/pixmaps/robo3t.png" },
     {"Sysprof", "sysprof "},
     {"Umbrello", "umbrello5  -caption Umbrello", "/usr/share/icons/hicolor/16x16/apps/umbrello.png" },
     {"UserFeedback Console", "UserFeedbackConsole", "/usr/share/icons/gnome/16x16/actions/search.png" },
     {"Zeal", "zeal ", "/usr/share/icons/hicolor/16x16/apps/zeal.png" },
     {"dmd/phobos documentation", "xdg-open /usr/share/d/html/d/spec/intro.html"},
     {"gitg", "gitg --no-wd ", "/usr/share/icons/hicolor/16x16/apps/org.gnome.gitg.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Audacious", "audacious ", "/usr/share/icons/hicolor/48x48/apps/audacious.png" },
     {"Brasero", "brasero ", "/usr/share/icons/hicolor/16x16/apps/brasero.png" },
     {"Clementine", "clementine ", "/usr/share/icons/hicolor/64x64/apps/clementine.png" },
     {"Dragon Player", "dragon ", "/usr/share/icons/hicolor/16x16/apps/dragonplayer.png" },
     {"Elisa", "elisa ", "/usr/share/icons/hicolor/16x16/apps/elisa.png" },
     {"HandBrake", "ghb "},
     {"JuK", "juk ", "/usr/share/icons/hicolor/16x16/apps/juk.png" },
     {"K3b", "k3b ", "/usr/share/icons/hicolor/16x16/apps/k3b.png" },
     {"Kamoso", "kamoso -qwindowtitle Kamoso", "/usr/share/icons/hicolor/16x16/apps/kamoso.png" },
     {"Kdenlive", "kdenlive ", "/usr/share/icons/hicolor/16x16/apps/kdenlive.png" },
     {"Kodi", "kodi", "/usr/share/icons/hicolor/16x16/apps/kodi.png" },
     {"Kwave Sound Editor", "kwave "},
     {"Lollypop", "lollypop ", "/usr/share/icons/hicolor/16x16/apps/org.gnome.Lollypop.png" },
     {"Music", "io.elementary.music ", "/usr/share/icons/hicolor/16x16/apps/multimedia-audio-player.svg" },
     {"OBS Studio", "obs", "/usr/share/icons/hicolor/256x256/apps/com.obsproject.Studio.png" },
     {"Peek", "peek"},
     {"PulseAudio Volume Control", "pavucontrol", "/usr/share/icons/gnome/16x16/apps/multimedia-volume-control.png" },
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"Qt V4L2 video capture utility", "qvidcap", "/usr/share/icons/hicolor/16x16/apps/qvidcap.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
     {"mpv Media Player", "mpv --player-operation-mode=pseudo-gui -- ", "/usr/share/icons/hicolor/16x16/apps/mpv.png" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Bulk Rename", "thunar --bulk-rename ", "/usr/share/icons/hicolor/16x16/apps/org.xfce.thunar.png" },
     {"Cairo-Dock", "cairo-dock", "/usr/share/pixmaps/cairo-dock.svg" },
     {"Cairo-Dock (Fallback Mode)", "cairo-dock -A", "/usr/share/pixmaps/cairo-dock.svg" },
     {"Discover", "plasma-discover ", "/usr/share/icons/hicolor/16x16/apps/plasmadiscover.png" },
     {"Dolphin", "dolphin ", "/usr/share/icons/gnome/16x16/apps/system-file-manager.png" },
     {"Fondo", "com.github.calo001.fondo", "/usr/share/icons/hicolor/16x16/apps/com.github.calo001.fondo.svg" },
     {"GParted", "/usr/bin/gparted ", "/usr/share/icons/hicolor/16x16/apps/gparted.png" },
     {"GSmartControl", "/usr/bin/gsmartcontrol_polkit", "/usr/share/icons/hicolor/16x16/apps/gsmartcontrol.png" },
     {"Grsync", "/usr/bin/grsync -i ", "/usr/share/icons/hicolor/128x128/apps/grsync.png" },
     {"Hardware Locality lstopo", "lstopo"},
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"KDE Partition Manager", "partitionmanager"},
     {"KDiskFree", "kdf -qwindowtitle KDiskFree", "/usr/share/icons/hicolor/16x16/apps/kdf.png" },
     {"KSysGuard", "ksysguard ", "/usr/share/icons/gnome/16x16/apps/utilities-system-monitor.png" },
     {"KSystemLog", "/usr/bin/kdesu ksystemlog -qwindowtitle KSystemLog"},
     {"KWalletManager", "kwalletmanager5 ", "/usr/share/icons/hicolor/16x16/apps/kwalletmanager.png" },
     {"Konsole", "konsole", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"KwikDisk", "kwikdisk -qwindowtitle KwikDisk", "/usr/share/icons/hicolor/16x16/apps/kwikdisk.png" },
     {"MEGAsync", "megasync", "/usr/share/icons/hicolor/16x16/apps/mega.png" },
     {"Manage Printing", "/usr/bin/xdg-open http://localhost:631/", "/usr/share/icons/hicolor/16x16/apps/cups.png" },
     {"OpenJDK Java 11 Console", "/usr/lib/jvm/java-11-openjdk/bin/jconsole", "/usr/share/icons/hicolor/16x16/apps/java11-openjdk.png" },
     {"OpenJDK Java 11 Shell", "xterm -e /usr/lib/jvm/java-11-openjdk/bin/jshell", "/usr/share/icons/hicolor/16x16/apps/java11-openjdk.png" },
     {"Oracle VM VirtualBox", "VirtualBox ", "/usr/share/icons/hicolor/16x16/mimetypes/virtualbox.png" },
     {"ROX Filer", "rox", "/usr/share/pixmaps/rox.png" },
     {"SpaceFM", "spacefm ", "/usr/share/icons/hicolor/48x48/apps/spacefm.png" },
     {"Terminator", "terminator", "/usr/share/icons/hicolor/16x16/apps/terminator.png" },
     {"Terminology", "terminology", "///usr/share/icons/hicolor/128x128/apps/terminology.png" },
     {"Thunar File Manager", "thunar ", "/usr/share/icons/hicolor/16x16/apps/org.xfce.thunar.png" },
     {"UXTerm", "uxterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"WoeUSB", "woeusbgui", "/usr/share/pixmaps/woeusbgui-icon.png" },
     {"XTerm", "xterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"Yakuake", "yakuake", "/usr/share/icons/hicolor/16x16/apps/yakuake.png" },
     {"conky", "conky --daemonize --pause=1"},
     {"emelFM2", "/usr/bin/emelfm2", "/usr/share/pixmaps/emelfm2.png" },
     {"lf", "xterm -e lf", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"nnn", "xterm -e nnn", "/usr/share/icons/hicolor/64x64/apps/nnn.png" },
     {"ranger", "xterm -e ranger", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"urxvt", "urxvt", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"urxvt (client)", "urxvtc", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"urxvt (tabbed)", "urxvt-tabbed", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
 }

xdgmenu = {
    {"Accessibility", menue0e4fc6213e8b3593495a7260c3a4c2e},
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Education", menude7a22a0c94aa64ba2449e520aa20c99},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

