#!/bin/sh

nitrogen --restore

megasync &
dropbox &
conky_reload &
compton --config $HOME/.config/compton.conf &
xdg_menu --format awesome --root-menu /etc/xdg/menus/arch-applications.menu > $HOME/.config/awesome/archmenu.lua
